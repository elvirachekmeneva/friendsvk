//
//  FriendsTableCell.m
//  FriendsVK
//
//  Created by Эльвира on 27.01.15.
//  Copyright (c) 2015 Эльвира. All rights reserved.
//

#import "FriendsTableCell.h"

@implementation FriendsTableCell

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)setImageToImageView:(UIImageView*)imgView withURL:(NSString*)URL activity:(UIActivityIndicatorView*) activity {
    
    UIImage* img = [[SDImageCache sharedImageCache] imageFromDiskCacheForKey:[NSString stringWithFormat:@"%@",URL]];
    
    if( img ){
        activity.hidden = YES;
        [activity stopAnimating];
        
        [imgView setImage:img];

    } else {
        [imgView sd_setImageWithURL:[NSURL URLWithString:URL] placeholderImage:nil completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL)
         {
             activity.hidden = YES;
             [activity stopAnimating];
             
             [[SDImageCache sharedImageCache] storeImage:image forKey:[NSString stringWithFormat:@"%@",URL] toDisk:YES];
         }];
    }
}

@end
