// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to Friend.h instead.

#import <CoreData/CoreData.h>

extern const struct FriendAttributes {
	__unsafe_unretained NSString *friends_owner;
	__unsafe_unretained NSString *lastname;
	__unsafe_unretained NSString *name;
	__unsafe_unretained NSString *photo;
	__unsafe_unretained NSString *userID;
} FriendAttributes;

extern const struct FriendRelationships {
	__unsafe_unretained NSString *user_photo;
} FriendRelationships;

extern const struct FriendUserInfo {
	__unsafe_unretained NSString *key;
} FriendUserInfo;

@class Photo;

@interface FriendID : NSManagedObjectID {}
@end

@interface _Friend : NSManagedObject {}
+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_;
+ (NSString*)entityName;
+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_;
@property (nonatomic, readonly, strong) FriendID* objectID;

@property (nonatomic, strong) NSString* friends_owner;

//- (BOOL)validateFriends_owner:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSString* lastname;

//- (BOOL)validateLastname:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSString* name;

//- (BOOL)validateName:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSString* photo;

//- (BOOL)validatePhoto:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSString* userID;

//- (BOOL)validateUserID:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) Photo *user_photo;

//- (BOOL)validateUser_photo:(id*)value_ error:(NSError**)error_;

@end

@interface _Friend (CoreDataGeneratedPrimitiveAccessors)

- (NSString*)primitiveFriends_owner;
- (void)setPrimitiveFriends_owner:(NSString*)value;

- (NSString*)primitiveLastname;
- (void)setPrimitiveLastname:(NSString*)value;

- (NSString*)primitiveName;
- (void)setPrimitiveName:(NSString*)value;

- (NSString*)primitivePhoto;
- (void)setPrimitivePhoto:(NSString*)value;

- (NSString*)primitiveUserID;
- (void)setPrimitiveUserID:(NSString*)value;

- (Photo*)primitiveUser_photo;
- (void)setPrimitiveUser_photo:(Photo*)value;

@end
