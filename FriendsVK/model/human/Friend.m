#import "Friend.h"

@interface Friend ()

// Private interface goes here.

@end

@implementation Friend

// Custom logic goes here.
+ (instancetype)friendWithID:(NSString *)ID
{
    Friend* item = [Friend MR_findFirstByAttribute:@"userID" withValue:ID];
    return item;
}
//+(void) createCreation:^(Friend * friend)  {
//    
//}
//
//class func create(creation:(pomodoro:Pomodoro)->Void, completion:(success:Bool?,error:NSError?)->Void) {
//    MagicalRecord.saveWithBlock({ (localContext) -> Void in
//        var pomodoro = Pomodoro.MR_createInContext(localContext) as Pomodoro
//        
//        creation(pomodoro: pomodoro)
//    }, completion: { (success, error) -> Void in
//        completion(success: success, error: error)
//    })
//}


@end
