#import "_Friend.h"

@interface Friend : _Friend {}
// Custom logic goes here.

+ (instancetype)friendWithID:(NSString *)ID;
@end
