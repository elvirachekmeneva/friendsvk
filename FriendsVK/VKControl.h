//
//  VKControl.h
//  FriendsVK
//
//  Created by Эльвира on 23.01.15.
//  Copyright (c) 2015 Эльвира. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>
#import "CoreData+MagicalRecord.h"
#import "KeychainItemWrapper.h"
#import "Friend.h"

#define VK_СURRENT_USER @"currentUserID"
@protocol VKControlDelegate <NSObject>

- (void)dismissLoginVC;
- (void)loadingFinished;

@end

@interface VKControl : NSObject <VKSdkDelegate>

@property (nonatomic, readonly) BOOL isLogined;
@property (nonatomic, strong) NSString* token;
@property (nonatomic, strong) NSString* userID;
@property (nonatomic, weak) id<VKControlDelegate> delegate;

+(instancetype)shared;
- (void)loadFriends;

@end
