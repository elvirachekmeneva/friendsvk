//
//  FriendsViewController.m
//  FriendsVK
//
//  Created by Эльвира on 23.01.15.
//  Copyright (c) 2015 Эльвира. All rights reserved.
//

#import "FriendsViewController.h"

@interface FriendsViewController ()

@end

@implementation FriendsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    if ( ![VKSdk wakeUpSession] )
    {
        [VKSdk forceLogout];
        LoginViewController * login = [self.storyboard instantiateViewControllerWithIdentifier:@"LoginViewController"];
        [self presentViewController:login animated:NO completion:nil];
    } else {
        self.isUpdating = YES;
        [[VKControl shared] loadFriends];
    }
    
    
}
- (void)viewDidLayoutSubviews {
    [super viewDidLayoutSubviews];
}
-(UIStatusBarStyle)preferredStatusBarStyle{
    return UIStatusBarStyleLightContent;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (IBAction)logOut:(id)sender {
    
    KeychainItemWrapper *keychainWrapper = [[KeychainItemWrapper alloc] initWithIdentifier:@"UserAuthToken" accessGroup:nil];
    [keychainWrapper resetKeychainItem];
    [VKSdk forceLogout];
    LoginViewController * login = [self.storyboard instantiateViewControllerWithIdentifier:@"LoginViewController"];
    [self presentViewController:login animated:NO completion:nil];
    
    

    
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    NSLog(@"count of elements %lu", (unsigned long)self.fetchedResultsController.fetchedObjects.count);
    return self.fetchedResultsController.fetchedObjects.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    NSString* identifier = @"FriendsTableCell";
    Friend * friend = self.fetchedResultsController.fetchedObjects[indexPath.row];
    FriendsTableCell* cell = [tableView dequeueReusableCellWithIdentifier:identifier];
    if (!cell) {
        cell = [[FriendsTableCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identifier];
    }
    cell.activity.hidden = NO;
    cell.friendPhoto.layer.cornerRadius = 30;
    cell.friendName.text = [NSString stringWithFormat:@"%@ %@", friend.name, friend.lastname];
    [cell setImageToImageView:cell.friendPhoto withURL:friend.photo activity:cell.activity];
    
    return cell;
}

-(void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    float maxTableViewOffset = 120.f;
    if ( !self.isUpdating && scrollView.contentOffset.y < - maxTableViewOffset )
    {
        self.isUpdating = YES;
        [[VKControl shared]loadFriends];

    }
    

}

- (void)setIsUpdating:(BOOL)isUpdating {
    _isUpdating = isUpdating;
    if (isUpdating) {
        [self startActivityIndicatorAnimation];
    } else {
        [self stopActivityIndicatorAnimation];
    }
}


- (void)startActivityIndicatorAnimation
{
    self.activity.alpha = 1;
    [self.activity startAnimating];
}

- (void) stopActivityIndicatorAnimation {
    self.activity.alpha = 0;
    [self.activity stopAnimating];
}


- (NSFetchedResultsController*) fetchedResultsController {
    if (!_fetchedResultsController && [[VKSdk getAccessToken] userId]) {
        _fetchedResultsController = [Friend MR_fetchAllGroupedBy:nil withPredicate:[NSPredicate predicateWithFormat:@"friends_owner == %@",[[VKSdk getAccessToken] userId]] sortedBy:@"name" ascending:YES];
        _fetchedResultsController.delegate = self;
    }
    return  _fetchedResultsController;
    

}

- (void)loadingFinished {
    self.isUpdating = NO;
    [self.tableView reloadData];
}

- (void)updateTable{
    [self.tableView reloadData];
}

- (void)controllerDidChangeContent:(NSFetchedResultsController *)controller {
    self.isUpdating = NO;
    [self.tableView reloadData];
}



@end
