//
//  FriendsTableCell.h
//  FriendsVK
//
//  Created by Эльвира on 27.01.15.
//  Copyright (c) 2015 Эльвира. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <SDWebImage/UIImageView+WebCache.h>

@interface FriendsTableCell : UITableViewCell
@property (strong, nonatomic) IBOutlet UIImageView *friendPhoto;
@property (strong, nonatomic) IBOutlet UILabel *friendName;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *activity;

- (void)setImageToImageView:(UIImageView*)imgView withURL:(NSString*)URL activity:(UIActivityIndicatorView*) activity;

@end
