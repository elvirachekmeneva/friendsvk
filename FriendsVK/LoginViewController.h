//
//  LoginViewController.h
//  FriendsVK
//
//  Created by Эльвира on 23.01.15.
//  Copyright (c) 2015 Эльвира. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "KeychainItemWrapper.h"



@interface LoginViewController : UIViewController <UIWebViewDelegate, VKSdkDelegate, VKControlDelegate>
@property (strong, nonatomic) IBOutlet UIWebView *webView;
@property (strong, nonatomic) IBOutlet UIActivityIndicatorView *indicator;

@end
