//
//  LoginViewController.m
//  FriendsVK
//
//  Created by Эльвира on 23.01.15.
//  Copyright (c) 2015 Эльвира. All rights reserved.
//

#import "LoginViewController.h"

@interface LoginViewController ()

@end

@implementation LoginViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [VKControl shared].delegate = self;
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (void)viewDidLayoutSubviews {
    [super viewDidLayoutSubviews];
}


-(NSDictionary *)paramsDictFromUrl:(NSURL*) url{
    NSMutableDictionary * params = [NSMutableDictionary dictionary];
    NSArray * split = [url.absoluteString componentsSeparatedByString:@"#"];
    if (split.count >= 2) {
        NSString * paramsString = split[1];
        NSArray * paramsArray = [paramsString componentsSeparatedByString:@"&"];
        if (paramsArray.count > 0) {
            for (NSString * param in paramsArray) {
                NSArray * splitParam = [param componentsSeparatedByString:@"="];
                if (splitParam.count > 1) {
                    params[splitParam[0]] = splitParam[1];
                }
            }
        }
    }
    return [params copy];
}

- (IBAction)vkAuth:(id)sender {
    NSArray * scope = @[@"friends"];
    [VKSdk authorize:scope revokeAccess:YES forceOAuth:YES inApp:NO];
}

- (void)dismissLoginVC {
    [self dismissViewControllerAnimated:YES completion:nil];
}



@end
