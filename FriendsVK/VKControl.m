//
//  VKControl.m
//  FriendsVK
//
//  Created by Эльвира on 23.01.15.
//  Copyright (c) 2015 Эльвира. All rights reserved.
//

#import "VKControl.h"

@implementation VKControl

+(instancetype)shared {
    static VKControl * instance;
    @synchronized(self) {
        if (!instance) {
            instance = [[VKControl alloc] init];
            [MagicalRecord setupCoreDataStackWithStoreNamed:@"model"];
        }
    }
    return instance;
}

- (BOOL)isLogined {
    KeychainItemWrapper *keychainWrapper = [[KeychainItemWrapper alloc] initWithIdentifier:@"UserAuthToken" accessGroup:nil];
    NSString* userId = [keychainWrapper objectForKey:(__bridge id)(kSecAttrAccount)];
    NSString* token = [keychainWrapper objectForKey:(__bridge id)(kSecValueData)];
    
    if (userId.length > 0 && token.length > 0) {
        self.userID = userId;
        self.token = token;
        return YES;
    }
    
    return NO;
}

- (NSString *)userID {
    if (!_userID) {
        KeychainItemWrapper *keychainWrapper = [[KeychainItemWrapper alloc] initWithIdentifier:@"UserAuthToken" accessGroup:nil];
        _userID = [keychainWrapper objectForKey:(__bridge id)(kSecAttrAccount)];
    }
    return _userID;
}

-(NSString *)token {
    if (!_token) {
        KeychainItemWrapper *keychainWrapper = [[KeychainItemWrapper alloc] initWithIdentifier:@"UserAuthToken" accessGroup:nil];
        _token = [keychainWrapper objectForKey:(__bridge id)(kSecValueData)];
    }
    return _token;
}



- (void)vkSdkReceivedNewToken:(VKAccessToken *)newToken {
    NSLog(@"new token");
    
    NSString* userID = newToken.userId;
    KeychainItemWrapper *keychainWrapper = [[KeychainItemWrapper alloc] initWithIdentifier:@"UserAuthToken" accessGroup:nil];
    [keychainWrapper setObject:userID forKey:(__bridge id)(kSecAttrAccount)];
    [keychainWrapper setObject:newToken.accessToken forKey:(__bridge id)(kSecValueData)];
    
    self.userID = userID;
    self.token = newToken.accessToken;
    [self.delegate dismissLoginVC];
    
    
}

- (void)loadFriends {
    NSLog( @"Start load friends" );
    
    NSDictionary* params  = @{ @"user_id": self.userID,
                               @"order" : @"name",
                               @"fields" : @"photo_100",
                               @"name_case" : @"nom"
                               };
    
    
    VKRequest* getNewsRequest = [VKRequest requestWithMethod:@"friends.get" andParameters:params andHttpMethod:@"GET"];
    
    [getNewsRequest executeWithResultBlock:^(VKResponse * response){
         NSLog(@"Friends json result: %@", response.json);
         
         NSDictionary* responseDictionary = (NSDictionary*)response.json;
         
         [self parseFriendsData:responseDictionary];
        [self.delegate loadingFinished];
         [[NSManagedObjectContext MR_defaultContext] MR_saveToPersistentStoreWithCompletion:^(BOOL success, NSError *error) {
             
          }];
         
     }
     
                                errorBlock:^(NSError * error) {
         if (error.code != VK_API_ERROR) {
             [error.vkError.request repeat];
         } else {
             NSLog(@"VK error: %@", error);
         }
     }];
}

- (void)parseFriendsData:(NSDictionary*)response
{
    
    for (NSDictionary* row in response[@"items"]) {
        NSString* userId = [NSString stringWithFormat:@"%@", row[@"id"]];
        Friend * item = [Friend friendWithID:userId];
        if (!item) {
            item = [Friend MR_createEntity];
            item.friends_owner = self.userID;
            item.userID = userId;
            item.name = row[@"first_name"];
            item.lastname = row[@"last_name"];
            item.photo = row[@"photo_100"];
        } else {
            item.userID = userId;
            item.name = row[@"first_name"];
            item.lastname = row[@"last_name"];
            item.photo = row[@"photo_100"];
        }
        
    }
    
    
}




@end
