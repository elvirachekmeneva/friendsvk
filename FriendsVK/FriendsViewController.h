//
//  FriendsViewController.h
//  FriendsVK
//
//  Created by Эльвира on 23.01.15.
//  Copyright (c) 2015 Эльвира. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "LoginViewController.h"
#import "VKControl.h"
#import "KeychainItemWrapper.h"
#import "FriendsTableCell.h"
#import "Friend.h"

@interface FriendsViewController : UIViewController <UITableViewDataSource, UITableViewDelegate, UIScrollViewDelegate, NSFetchedResultsControllerDelegate, VKControlDelegate>
@property (strong, nonatomic) IBOutlet UIActivityIndicatorView *activity;
@property (strong, nonatomic) IBOutlet UITableView *tableView;
@property (nonatomic, assign) BOOL isUpdating;
@property (nonatomic, strong) NSFetchedResultsController* fetchedResultsController;

@end
