//
//  AppDelegate.h
//  FriendsVK
//
//  Created by Эльвира on 22.01.15.
//  Copyright (c) 2015 Эльвира. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "VKSdk.h"

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

